#!/usr/bin/env ruby

=begin
Interpret configurations of port forwarding and shared folders written in Vagrantfile,
then run VBoxManage commands and ssh commands to configure them.

Tested in Ruby 2.4.

Copyright 2017 Yuji Yamamoto

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=end

require 'ostruct'
require 'shellwords'

module Vagrant
  def self.configure(*_args, &block)
    @config = Config.new
    block.call @config
  end

  class << self
    attr_reader :config
  end

  class Config < ::OpenStruct
    def initialize
      super
      self.vm = VM.new
      self.ssh = OpenStruct.new
    end

    class VM < ::OpenStruct
      attr_reader :synced_folders

      def initialize
        super
        @network = OpenStruct.new
        @synced_folders = []
      end

      def network(config_type = nil, **args)
        if config_type
          @network[config_type] ||= []
          @network[config_type] << OpenStruct.new(args)
        else
          @network
        end
      end

      def synced_folder(host_path, guest_path, disabled: false)
        if disabled
          @synced_folders.delete_if {|(h, g)| h == host_path && g == guest_path }
        else
          @synced_folders << [host_path, guest_path]
        end
      end

      def provider(*_args)
        # Ignore any arguments. Because I don't need them!
      end

      def provision(*_args)
        # Ignore any arguments. Because I don't need them!
      end
    end
  end
end

load './Vagrantfile'

require 'optparse'

=begin
$ VBoxManage.exe sharedfolder --help
Oracle VM VirtualBox Command Line Management Interface Version 5.1.22
(C) 2005-2017 Oracle Corporation
All rights reserved.

Usage:

VBoxManage sharedfolder     add <uuid|vmname>
                            --name <name> --hostpath <hostpath>
                            [--transient] [--readonly] [--automount]

VBoxManage sharedfolder     remove <uuid|vmname>
                            --name <name> [--transient]

$ VBoxManage.exe controlvm --help
Oracle VM VirtualBox Command Line Management Interface Version 5.1.22
(C) 2005-2017 Oracle Corporation
All rights reserved.

Usage:

VBoxManage controlvm        <uuid|vmname>
                            natpf<1-N> [<rulename>],tcp|udp,[<hostip>],
                                        <hostport>,[<guestip>],<guestport> |
                            natpf<1-N> delete <rulename> |
=end

if __FILE__ == $PROGRAM_NAME
  opts = ARGV.getopts('nu:s:p:', 'dry-run', 'vm:', 'ssh-host:', 'ssh-port:')
  vm_id = opts['u'] || opts['vm']
  ssh_host = opts['s'] || opts['ssh-host'] || 'localhost'
  ssh_port = opts['p'] || opts['ssh-port'] || '22'

  if vm_id.nil?
    fail "Specify target virtual machine's UUID or name with -u or --vm option."
  end

  run =
    if opts['n'] || opts['dry-run']
      -> (*args){ puts args.join(' ') }
    else
      -> (*args) do
        print "Running '#{Shellwords.join args}' - "
        if system(*args)
          puts "SUCCESS"
        else
          puts "FAILED"
        end
      end
    end

  config = Vagrant.config

  config.vm.synced_folders.each do|(host, guest)|
    run[
      "VBoxManage.exe", "sharedfolder",
      "add", vm_id,
      "--name", File.basename(guest),
      "--hostpath", host,
    ]
  end

  config.vm.network.forwarded_port.each do|port_forwarding|
    run[
      "VBoxManage.exe",
      "controlvm", vm_id,
      "natpf1", "tcp#{port_forwarding.host},tcp,127.0.0.1,#{port_forwarding.host},,#{port_forwarding.guest}"
    ]
  end

  run_ssh_arg = ["ssh", "-p", ssh_port, "#{config.ssh.username}@#{ssh_host}"]

  install_vbox_mod = "echo vboxsf | sudo tee /etc/modules-load.d/virtualbox.conf"
  run[*(run_ssh_arg + [install_vbox_mod])]

  config.vm.synced_folders.each do|(_, guest)|
    guest.sub!(%r'/$', '')
    guest_name = File.basename(guest)
    append_to_fstab = <<-"END"
    grep -Fqe '#{guest_name}' /etc/fstab || echo '#{guest_name} /media/sf_#{guest_name} vboxsf fmode=0777,dmode=0777 0 0' | sudo tee -a /etc/fstab
    END
    run[*(run_ssh_arg + [append_to_fstab])]
    run[*(run_ssh_arg + ["mkdir", "-p", File.dirname(guest)])]
    run[*(run_ssh_arg + ["ln", "-s", "/media/sf_#{guest_name}", guest])]
  end
end
